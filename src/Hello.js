import React, { Component } from 'react';

class Hello extends Component{

  render(){
    const printvalue=this.props.users.map((user,i)=>{
    		return (<tr key={this.props.users[i].id }><td><div  className="tooltip"><div className="name">{this.props.users[i].name}</div>
  <span className="tooltiptext">
    <strong>Street</strong>:{this.props.users[i].address.street}<br></br>
    <strong>Suite</strong>:{this.props.users[i].address.suite}<br></br>
    <strong>City</strong>:{this.props.users[i].address.city}<br></br>
    <strong>Zip</strong>:{this.props.users[i].address.zipcode}
  </span>
</div>
                </td> 
                <td className="uname">
                  ({this.props.users[i].username})
                </td>
              </tr>
              
              )
    	});
    return(<tbody>{printvalue}</tbody>)
  }
}

export default Hello;