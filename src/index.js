import React, { Component } from 'react';
import { render } from 'react-dom';
import Hello from './Hello';
import './style.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      users: '',
      flag:0
    };
  }
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => this.setState({users: data,flag:1 }));     
  }


  render() {
    let value="";
    let value2="Users(loading)";
    if(this.state.flag==1)
    {
      value=<Hello users={this.state.users} />
      value2="Users";
    }
      
    return (
      <div>
            {value2}

      <div className="whole">
        <table>
            {value}
        </table>
      </div>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
